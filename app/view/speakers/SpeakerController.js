Ext.define('ConferenceApp.view.speakers.SpeakerController', {
    extend: 'Ext.app.ViewController',
    util: ConferenceApp.util.Util,
    requires: [
    'Ext.MessageBox',
    'ConferenceApp.view.speakers.AddSpeaker'
    ],

    alias: 'controller.speaker',
    
    /*
     *  Search speakers  
     */
    searchSpeakers : function(input,e) {
        var inputVal = input.target.value,
        speakerStore = Ext.getStore('Speakers');
        
        // Clear the filter if no value is entered (or if the field is cleared)
        if(!inputVal.length) {
            speakerStore.clearFilter();
            return;
        }
        
        // Filter the store
        speakerStore.filter([{
            property: 'name',
            value: inputVal
        }]);
    },
    /*
     *   Open add speaker popup window
     */
    addSpeaker: function() {
        var addSpeakersWindow = this.lookupReference('addSpeakersWindow');

        if (!addSpeakersWindow) {
            addSpeakersWindow = new ConferenceApp.view.speakers.AddSpeaker();
            this.getView().add(addSpeakersWindow);
        }

        addSpeakersWindow.show();
    },
    /*
     *   Hide and reset the add speaker form
     */
    onAddSpeakerFormCancel : function() {
        
        this.lookupReference('addSpeakerForm').getForm().reset();
        this.lookupReference('addSpeakersWindow').hide();
    },
    /*
     *  Add speaker field in form
     */
    addSpeakerField: function(options, newItems) {
        var items; 
        
        if(newItems){
            items = options.field.add(newItems);
        } else {
            items = options.field.add([{
                xtype: 'textfield',
                emptyText: options.emptyText,
                columnWidth: 0.92,
                fieldLabel: ' ',
                labelSeparator: '',
                name: options.name,
                labelWidth: 100,
                cls: 'add-speaker-new-field'
            },{
                xtype: 'button',
                cls: 'speaker-add-btn add-speaker-new-field',
                handler: options.handler,
                tooltip: 'Delete',
                glyph: 0xf068,
                columnWidth: 0.1
            }]);
        }
        
        // Increase the height of the item as its a vbox layout
        options.field.setHeight(options.field.getHeight() + 50 );
        
        // Stop the scolling by focusing on the 1st textfield
        items[0].focus();
    },
    /*
     *  Delete newly added field
     */
    deleteSpeakerField: function (field, fieldAction, btn) {
        // Decrease the height
        field.setHeight(field.getHeight() - 50 );
        
        // Stop the scolling by focusing on the 1st textfield
        field.down('textfield[action="' + fieldAction + '"]').focus();
        
        // Destroy the textfield and button
        btn.prev().destroy();
        btn.destroy();
    },
    /*
     *  Add speaker websites in form
     */
    addSpeakerWebsites: function() {
        var me = this;
        
        me.addSpeakerField({
            field : me.lookupReference('website_field'),
            emptyText : 'Enter websites',
            name: 'websites',
            handler : 'deleteSpeakerWebsiteField'
        });
    },
    /*
     *  Delete newly added website field
     */
    deleteSpeakerWebsiteField: function (btn) {
        this.deleteSpeakerField(this.lookupReference('website_field'),"website-field", btn);
    },
    /*
     *  Add speaker websites in form
     */
    addSpeakerBlogs: function() {
        var me = this;
        
        me.addSpeakerField({
            field : me.lookupReference('blog_field'),
            emptyText : 'Enter blogs',
            name: 'blogs',
            handler : 'deleteSpeakerBlogField'
        });
    },
    /*
     *  Delete newly added blog field
     */
    deleteSpeakerBlogField: function (btn) {
        this.deleteSpeakerField(this.lookupReference('blog_field'),"blog-field", btn);
    },
    /*
     *  Add speaker social in form
     */
    addSpeakerSocials: function() {
        var me = this,
        newSocialField = [{
            xtype: 'textfield',
            fieldLabel: ' ',
            labelSeparator: '',
            emptyText: 'Enter social networks like fb, g+, linkedIn, etc',
            name: 'socials',
            columnWidth: 0.72,
            cls: 'add-speaker-new-field'
        },{
            xtype: 'combo',
            columnWidth: 0.2,
            valueField: 'value',
            name: 'social_key',
            editable: false,
            value: 'fb',
            cls: 'add-speaker-new-field',
            store: { 
                fields: ['text', 'value'],
                data : [{
                    text: 'Facebook',
                    value: 'fb'
                },{
                    text: 'Twitter',
                    value: 'tw'
                },{
                    text: 'LinkedIn',
                    value: 'ln'
                },{
                    text: 'Google+',
                    value: 'g+'
                }]
            }
        },{
            xtype: 'button',
            cls: 'speaker-add-btn add-speaker-new-field',
            handler: 'deleteSpeakerSocialField',
            tooltip: 'Delete',
            glyph: 0xf068,
            columnWidth: 0.1
        }];
        
        me.addSpeakerField({
            field : me.lookupReference('social_field')
        }, newSocialField);
    },
    /*
     *  Delete newly added social field
     */
    deleteSpeakerSocialField: function (btn) {
        // Destroy the extra combobox
        btn.prev().destroy();
        this.deleteSpeakerField(this.lookupReference('social_field'), "social-field", btn);
    },
    /*
     *  Change new speaker's profile picture
     */
    changeSpeakerProfileImg : function(btn, val) {
        var me = this,
        speakerProfileImgField = me.lookupReference('newSpeakerProfileImg'),
        file = btn.fileInputEl.dom.files[0],
        fileConfig = {
            file: file,
            modelData: null,
            fieldName: null,
            imageField : speakerProfileImgField,
            displayField: null
        };

        me.util.readImage(fileConfig, btn, false);
    },
    /*
     *   Add the speaker and show the new speaker on list
     */
    onAddSpeakerFormSubmit: function() {
        var addSpeakersForm = this.lookupReference('addSpeakerForm'),
        addSpeakersFormValues =  addSpeakersForm.getValues(),
        socials = {},
        location = {
            country : addSpeakersFormValues['country'],
            state : addSpeakersFormValues['state'],
            city : addSpeakersFormValues['city']
        };
       
        // Formatting socials object
        if(Ext.isArray(addSpeakersFormValues['socials'])){
            addSpeakersFormValues['socials'].forEach(function(value, index){
                socials[addSpeakersFormValues['social_key'][index]] = value;
            });
        } else if(addSpeakersFormValues['socials']) {   
            socials[addSpeakersFormValues['social_key']] = addSpeakersFormValues['socials'];  
        }
            
        console.log('Form data = ', addSpeakersFormValues, socials, location);
     
    }
});
